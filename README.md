### **Customized Auto-complete README**

Welcome to the Customized Auto-complete README! This document provides an overview of the problem statement, input/output format, implementation details, and complexity requirements for the implementation of a customized auto-complete feature using a Trie data structure.

### Problem Statement

Alice wants to modify the auto-complete feature on her phone to display only one word, along with its definition and the number of words in the dictionary that have the same prefix as the entered word. She has a dictionary containing English words, their frequencies, and definitions. I am helping Alice to implement this customized auto-complete feature efficiently.

### Input

I was provided with a file `Dictionary.txt` containing 3,000 words, their frequencies, and definitions. Each word in the dictionary consists of only lowercase English letters. I used the provided `load_dictionary(filename)` function to read the input file. The function returns a list of lists, where each inner list represents a word entry in the format `[word, definition, frequency]`.

### Output

I implemented a class named `Trie` with the following methods:

1. **`__init__(self, Dictionary)`**: Initializes a Trie based on the provided dictionary.

2. **`prefix_search(self, prefix)`**: Takes a prefix as input and returns a list `[word, definition, num_matches]`, where:
   - `word` is the word with the highest frequency among words that match the prefix.
   - `definition` is the definition of the word from the dictionary.
   - `num_matches` is the number of words in the dictionary that have the input prefix.

If there are multiple words with the highest frequency, the program returns the alphabetically smallest word. If no word matches the prefix, the program returns `[None, None, 0]`.

### Implementation

You need to create a Trie data structure to efficiently handle prefix matching. The Trie is constructed in the `__init__` method using the provided dictionary. The `prefix_search` method performs a prefix search in the Trie and returns the required output format.

### Sample Execution

Below is a sample execution of the program for different inputs:

```python
myTrie = Trie(Dictionary)
print(myTrie.prefix_search("a"))      # Output: [’aberr’, ’To wander; to stray. [Obs.] Sir T. Browne.’, 3000]
print(myTrie.prefix_search("an"))     # Output: [’andantino’, ’Rather quicker than andante; between that allegretto.’, 205]
print(myTrie.prefix_search("ana"))    # Output: [’analeptic’, ’Restorative; giving strength after disease. -- n.’, 161]
print(myTrie.prefix_search("anac"))   # Output: [’anaclastic’, ’Produced by the refraction of light, as seen through water; as, anaclastic curves.’, 24]
print(myTrie.prefix_search("anace"))  # Output: [None, None, 0]
print(myTrie.prefix_search(""))      # Output: [’aberr’, ’To wander; to stray. [Obs.] Sir T. Browne.’, 3000]
```

### Complexity/Implementation Requirement

- The Trie construction has a worst-case time complexity of O(T) and a worst-case space complexity of O(T), where T is the total number of characters in the dictionary.
- The `prefix_search` function has a complexity of O(M + N), where M is the length of the prefix entered by the user and N is the total number of characters in the word with the highest frequency and its definition.

### Notes

- I have implement my own Trie and not used any publicly available implementations.
- I have not not used Python dictionaries or linked lists for implementing the Trie; use arrays (lists in Python) as nodes.
- Ensuring my program follows the specified output format for compatibility with the automated testing script.

For any further assistance or inquiries, please reach out to the developer.

*Developer: Anushka Reddy*
*Contact: anushkar614@gmail.com*
